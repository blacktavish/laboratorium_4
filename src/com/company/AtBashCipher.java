package com.company;

public class AtBashCipher implements Cipher{

    String alphabetUpper = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    String alphabetLower = "abcdefghijklmnopqrstuvwxyz";
    String reverseUpper = "ZYXWVUTSRQPONMLKJIHGFEDCBA";
    String reverseLower = "zyxwvutsrqponmlkjihgfedcba";


    @Override
    public String decode(String message) {
        return encode(message);
    }

    @Override
    public String encode(String message) {

        String encodeMessage = "";

        for(char letter: message.toCharArray()){
            if(Character.isUpperCase(letter)){
                int letterPosition = alphabetUpper.indexOf(letter);
                char encodeLetter = reverseUpper.charAt((letterPosition));
                encodeMessage += encodeLetter;
            }
            else if(Character.isLowerCase(letter)){
                int letterPosition = alphabetLower.indexOf(letter);
                char encodeLetter = reverseLower.charAt((letterPosition));
                encodeMessage += encodeLetter;

            }
            else if(letter  == ' '){
                encodeMessage += " ";
            }
            else{
                encodeMessage = "Only letters!";
            }
        }
        return encodeMessage;
    }
}
